/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9214483779323954, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.4748923959827834, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9999384293322661, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.8940138485593031, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.7256546644844517, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.0, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.7930686406460296, 500, 1500, "me"], "isController": false}, {"data": [0.9587210160980653, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.795218990304246, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.8988814317673378, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.002336448598130841, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 58783, 0, 0.0, 254.44926254188996, 11, 13475, 61.0, 635.0, 1032.0, 2041.8700000000208, 194.11669523120767, 668.3063930910137, 240.1286511117881], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 1394, 0, 0.0, 1074.980631276902, 245, 2620, 1058.5, 1442.5, 1558.5, 1866.1499999999999, 4.637901033712949, 2.9303925476682404, 5.738496689174127], "isController": false}, {"data": ["getLatestMobileVersion", 32483, 0, 0.0, 45.71557430040291, 11, 1070, 27.0, 90.0, 149.0, 219.0, 108.30518703258525, 72.4502471848837, 79.85392207968933], "isController": false}, {"data": ["findAllConfigByCategory", 4477, 0, 0.0, 333.79696225150826, 25, 1719, 219.0, 721.0, 940.0, 1290.6600000000008, 14.924179036812086, 16.87714777795742, 19.383943475546946], "isController": false}, {"data": ["getNotifications", 2444, 0, 0.0, 611.8412438625202, 66, 2490, 495.0, 1406.0, 1515.5, 1761.6500000000005, 8.146422273998448, 69.14116795246142, 9.061303681722881], "isController": false}, {"data": ["getHomefeed", 139, 0, 0.0, 10850.24460431655, 3287, 13475, 11126.0, 12128.0, 12350.0, 13346.999999999998, 0.45901401148525706, 8.027814386415498, 2.2968630809086497], "isController": false}, {"data": ["me", 2972, 0, 0.0, 503.1705921938082, 63, 2009, 373.5, 1167.7000000000003, 1344.3999999999996, 1588.54, 9.906270415850033, 13.043651431026092, 31.392429198665386], "isController": false}, {"data": ["getAllClassInfo", 6771, 0, 0.0, 220.6102495938562, 23, 1519, 162.0, 453.8000000000002, 615.0, 969.5599999999995, 22.57308498828173, 13.050064758850375, 57.953750424016285], "isController": false}, {"data": ["findAllChildrenByParent", 2991, 0, 0.0, 499.7883650952863, 67, 2180, 351.0, 1200.0, 1381.8000000000002, 1657.2399999999998, 9.969833836102731, 11.196590733904435, 15.928367339711006], "isController": false}, {"data": ["findAllSchoolConfig", 4470, 0, 0.0, 334.37158836689014, 35, 1721, 233.0, 717.0, 919.0, 1242.0, 14.900745037251863, 324.9642950897545, 10.928183127906395], "isController": false}, {"data": ["getChildCheckInCheckOut", 642, 0, 0.0, 2332.0451713395605, 1324, 4417, 2289.5, 2977.4, 3128.9500000000003, 3455.57, 2.1353094369368617, 142.55484280987764, 9.82575983090478], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 58783, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
